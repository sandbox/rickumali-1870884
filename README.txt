drush_comments
--------------

INTRO
-----

Approving comments can be a great big pain, especially if your site
receives its share of spam comments.

The UI to approve comments forces administrators to select comments to
be deleted, but working on several hundred comments can be tedious.
The UI allows for batch approvals and deletions on at most 50 comments
at a time.

This drush command ('module') attempts to ease the comment approval
process by providing the ability to download a human-readable file
that an administrator can quickly scan. This file can then be
processed by drush to quickly delete or approve the comments.

For more help, after installing this module, type:

drush help --filter (then select 'Comments commands')

INSTALL
-------

Copy comments.drush.inc to any of the following:

  1. A .drush folder in your HOME folder.
  2. Anywhere in a folder tree below an active module on your site.
  3. /usr/share/drush/commands (configurable)
  4. In an arbitrary folder specified with the --include option.

AUTHOR
------

Rick Umali: rickumali@gmail.com
Drupal:     http://drupal.org/user/207723/
Tech BLOG:  http://tech.rickumali.com/
Twitter:    @rickumali
