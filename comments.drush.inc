<?php
/**
 * @file
 * This is "drush comments", drush commands for handling comments.
 *
 * These drush commands are meant to expedite the comment approval
 * process.
 *
 * You can copy this file to any of the following
 *   1. A .drush folder in your HOME folder.
 *   2. Anywhere in a folder tree below an active module on your site.
 *   3. /usr/share/drush/commands (configurable)
 *   4. In an arbitrary folder specified with the --include option.
 */

/**
 * Implements hook_drush_help().
 */
function comments_drush_help($section) {
  switch ($section) {
    case 'drush:get-all-unapproved':
      return dt("This command outputs all unapproved comments as a CSV row.");

    case 'drush:get-count-unapproved':
      return dt("This command displays the count of unapproved comments.");

    case 'drush:delete-unapproved':
      return dt("This command deletes unapproved comments. Does --all by default.");

    // The 'title' meta item is used to name a group of
    // commands in `drush help`.
    case 'meta:comments:title':
      return dt("Comments commands");

    // The 'summary' meta item is displayed in `drush help --filter`,
    // and is used to give a general idea what the commands in this
    // command file do, and what they have in common.
    case 'meta:comments:summary':
      return dt("Command line inspection and approval of comments.");

  }
}

/**
 * Implements hook_drush_command().
 */
function comments_drush_command() {
  $items = array();

  // Both commands need to be run from within a site
  // (i.e. DRUSH_BOOTSTRAP_DRUPAL_FULL).
  $items['get-all-unapproved'] = array(
    'description' => "This command outputs all unapproved comments as a CSV row.",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  $items['get-count-unapproved'] = array(
    'description' => "This command displays the count of unapproved comments.",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  $items['delete-unapproved'] = array(
    'description' => "This command deletes unapproved comments. --all is default.",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(
      'all' => 'This is the default. It deletes all unapproved comments.',
      'list' => 'A comma delimited list of comment IDs (as returned from get-all-unapproved). Example: --list=215,254,301',
      'file' => 'A comma delimited file containing a list of IDs (in the first column) to delete. Example: file=/tmp/ids-to-delete.txt',
    ),

  );

  $items['docs-comments'] = array(
    'description' => 'drush_comments: Why and how to properly use this command.',
    'hidden' => TRUE,
    'topic' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'callback' => 'drush_print_file',
    'callback arguments' => array(dirname(__FILE__) . '/comments-topic.txt'),
  );


  return $items;
}

/**
 * This replaces all commas in $s with a space.
 */
function _remove_any_commas($s) {
  return str_replace(',', ' ', $s);
}

/**
 * This code retrieves all unapproved comments for the site.
 *
 * Look at modules/comment/comment.admin.inc for the original query.
 */
function _get_all_unapproved_comments() {
  $query = db_select('comment', 'c');
  $query->join('node', 'n', 'n.nid = c.nid');
  $query->addField('n', 'title', 'node_title');
  $query->addTag('node_access');
  return ($query->fields('c', array('cid', 'subject', 'name', 'changed'))
    ->condition('c.status', COMMENT_NOT_PUBLISHED)
    ->execute());
}

/**
 * This code retrieves all unapproved comments from the file specified.
 *
 * It is _expected_ to be a comma-separated file, where the ID will be 
 * in the first column. 
 */
function _get_unapproved_comments_from_file($filename) {
  $contents = file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
  if ($contents != TRUE || count($contents) == 0) {
    return array();
  }
  $cids = array();
  foreach ($contents as $row) {
    $row_contents = explode(",", $row);
    $cids[] = $row_contents[0];
  }
  return $cids;
}

/**
 * This generates an array of valid comment IDs.
 *
 * array_intersect() will return all of the $cids that are in the 
 * array of $unapproved_cids (i.e. all the valid unapproved comments).
 */
function _generate_valid_cids($cids, $unapproved_cids) {
  return array_intersect($cids, $unapproved_cids);
}

/**
 * Displays a message about the comment IDs.
 */
function _display_cids($cids) {
  if (count($cids) == 0) {
    drush_print("Some or all comment ID(s) are not unapproved comments. Compare your list with output of get-all-unapproved.");
  }
  else {
    $count = count($cids);
    drush_print("This command will delete $count comment(s) with these IDs: ");
    drush_print(implode(",", $cids));
  }
}

/**
 * This is the get-all-unapproved command.
 *
 * @see drush_invoke()
 * @see drush.api.php
 * @see comment_admin_overview()
 */
function drush_comments_get_all_unapproved() {

  $all_unapproved_comments = _get_all_unapproved_comments();

  foreach ($all_unapproved_comments as $row) {
    $cids[] = $row->cid;
    $node_titles[] = $row->node_title;
  }
  $comments = comment_load_multiple($cids);

  foreach ($comments as $comment) {
    // Remove the first node title from the node_titles array and attach to
    // the comment.
    $comment->node_title = array_shift($node_titles);

    // Print each comment with a comma separated row.
    //
    // NOTE: The preg_replace ensures that there all newlines and white
    // spaces are collapsed.
    drush_print(
      implode(",", array_map("_remove_any_commas", array(
          $comment->cid,
          $comment->subject,
          preg_replace(
            '/\s+/',
            ' ',
            truncate_utf8($comment->comment_body[LANGUAGE_NONE][0]['value'], 128)
          ),
          $comment->name,
          $comment->node_title,
          format_date($comment->changed, 'short'),
          )
        )
      )
    );
  }
}

/**
 * This is the get-count-unapproved command.
 *
 * @see drush_invoke()
 * @see drush.api.php
 * @see comment_count_unpublished()
 */
function drush_comments_get_count_unapproved() {
  drush_print(comment_count_unpublished());
}

/**
 * This is the delete-unapproved command. 
 *
 * This command only removes a comment if it's an unapproved comment.
 *
 * @see drush_invoke()
 * @see drush.api.php
 * @see comment_delete_multiple()
 */
function drush_comments_delete_unapproved() {
  $cids = array();

  // Get all the unapproved comments from the system.
  // This is done regardless of the option because we're
  // only going to remove comments if they are in this
  // list.
  $all_unapproved_comments = _get_all_unapproved_comments();
  foreach ($all_unapproved_comments as $row) {
    $unapproved_cids[] = $row->cid;
  }
  if (count($unapproved_cids) == 0) {
    drush_print("There are no unapproved comments to delete!");
    return;
  }

  $cids_list = array();
  if ($filename = drush_get_option('file')) {
    drush_print("Attempting to read $filename.");
    $cids_list = _get_unapproved_comments_from_file($filename);
  }
  elseif ($raw_cids_list = drush_get_option('list')) {
    // Let's try to get an array out of the value passed
    // to the --list switch. drush_get_option can return
    // a boolean if --list is passed in without any values.
    if (!is_bool($raw_cids_list)) {
      $cids_list = explode(",", $raw_cids_list);
    }
  }
  else {
    // Neither --list or --file passed in, so let's delete all
    // of the unapproved comments.
    $cids_list = $unapproved_cids;
  }

  if (count($cids_list) == 0) {
    drush_print("There are no comment IDs passed in!");
  }
  else {
    $cids = _generate_valid_cids($cids_list, $unapproved_cids);
    _display_cids($cids);
  }

  if ((count($cids) > 0) && !drush_confirm(dt('Do you really want to continue?'))) {
    return drush_user_abort();
  }
  comment_delete_multiple($cids);
}
